# Projet Blog PHP

Créer un blog en PHP en utilisant le framework Symfony 4.3 et PDO

* Fonctionnalités attendues
    * Créer des articles
    * Consulter la liste des articles
    * Consulter un article spécifique
    * Modifier ou supprimer un article
* Maquetter l'application
    * Faire les wireframes des pages de l'application
* Utiliser Mariadb / PDO pour les bases de données (pas d'ORM) en se basant sur le diagramme de classe fourni
    * Créer la base de donnée du projet et la table correspondante à l'entité avec des données de tests dedans sous forme de script qui pourra remettre la bdd en état
    * Créer la classe entité correspondant à la table
    * Créer une classe d'accès au données en utilisant PDO (DAO / Repository)
* Créer un projet Symfony et coder l'application avec le framework
    * Créer des controllers et des templates
    * Créer une classe Form pour le Post
    * Utiliser bootstrap pour les templates (comme d'hab on veut une appli responsive)
    * Dans les controllers, utiliser les DAO que vous avez fait

![Diagramme de la classe Post](./Uml/blog.png)

Fonctionnalités bonus : Faire des tests fonctionnels de l'appli / Permettre des commentaires sur les posts / Faire un compteur de vue sur les posts

### Blog de voyage (In my steps)

Pour ce projet, j'ai voulu creer un blog de voyage. 
Pour permettre de partager ces experiences de voyages!

