<?php

namespace App\Entity;

use DateTime;

class Post
{
    private $id;
    private $title;
    private $author;
    private $postDate;
    private $content;
    private $imgPath;

    public function __construct(string $title, string $author, string $postDate= 'now', string $content, string $imgPath, int $id = null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->author = $author;
        $this->postDate = new DateTime($postDate);
        $this->content = $content;
        $this->imgPath = $imgPath;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }
    public function setTitle(string $title)
    {
        $this->title = $title;
    }
    public function getAuthor(): ?string
    {
        return $this->author;
    }
    public function setAuthor(string $author)
    {
        $this->author = $author;
    }
    public function getPostDate(): ?\DateTime
    {
        return $this->postDate;
    }
    public function setPostDate(\DateTime $postDate): void
    {
        $this->postDate = $postDate;
    }
    public function getContent(): ?string
    {
        return $this->content;
    }
    public function setContent(string $content)
    {
        $this->content = $content;
    }
    public function getImgPath(): ?string
    {
        return $this->imgPath;
    }
    public function setImgPath(string $imgPath)
    {
        $this->imgPath = $imgPath;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
