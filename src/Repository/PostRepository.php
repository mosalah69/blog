<?php

namespace App\Repository;

use App\Entity\Post;
use DateTime;

class PostRepository
{


    private $pdo;
    public function __construct()
    {
        $this->pdo = new \PDO(
            'mysql:host='.$_ENV['DATABASE_HOST'].';dbname=' . $_ENV['DATABASE_NAME'],
            $_ENV['DATABASE_USERNAME'],
            $_ENV['DATABASE_PASSWORD'],
            [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]
        );
    }
    public function findAll(): array
    {
        $query = $this->pdo->prepare('SELECT * FROM post');
        $query->execute();
        $results = $query->fetchAll();
        $list = [];
        foreach ($results as $line) {
            $post = $this->sqlToPost($line);
             $list[] = $post;
     }
        return $list;
    }

    public function add(Post $post): void
    {
        $now = new DateTime();
        $query = $this->pdo->prepare('INSERT INTO post (title, author, content, postDate, imgPath) VALUES (:title,:author,:content, :postDate, :imgPath)');

        $query->bindValue(':title', $post->getTitle(), \PDO::PARAM_STR);
        $query->bindValue(':author', $post->getAuthor(), \PDO::PARAM_STR);
        $query->bindValue(':content', $post->getContent(), \PDO::PARAM_STR);
        $query->bindValue(':postDate', $now->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
        $query->bindValue(':imgPath', $post->getImgPath(), \PDO::PARAM_STR);

        $query->execute();
        $post->setId(intval($this->pdo->lastInsertId()));
    }

    public function findById(int $id): ?Post
    {
        $query = $this->pdo->prepare('SELECT * FROM post WHERE id=:idPlaceholder');
        $query->bindValue(':idPlaceholder', $id, \PDO::PARAM_INT);
        $query->execute();
        $line = $query->fetch();
        if ($line) {
            return $this->sqlToPost($line);
        }
        return null;
    }
    private function sqlToPost(array $line): Post
    {
        //$resultDate = \DateTime::createFromFormat('Y-m-d H:i:s', $line['postDate']);
        // $resultDate = new \DateTime($line['postDate']);
        return new Post($line['title'], $line['author'], $line['postDate'], $line['content'], $line['imgPath'], $line['id']);
    }

    public function deleteArticle(int $id)
    {

        $delete = $this->pdo->prepare('DELETE FROM post WHERE id=:id');
        $delete->bindValue('id', $id, \PDO::PARAM_INT);
        $delete->execute();
    }
}