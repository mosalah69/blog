<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;




class PostController extends AbstractController
{

    /**
     * @Route ("add-article", name="add-article")
     */
    public function addArticle(Request $request)
    {
        $post = null;
        $title = $request->get("title");
        $author = $request->get("author");
        $content = $request->get("content");
        $imgPath = $request->get("imgPath");

        if ($title && $author && $content)/* && $imgPath)*/ {
            
            $post = new Post($title, $author, 'now', $content, $imgPath);
            $postRepository = new PostRepository();
            $postRepository->add($post);
            
            return $this->redirectToRoute('blog');
        }

        return $this->render('add.html.twig', [
            'post' => $post
        ]);
    }



    // /**
    // * @Route("/blog_old", name="blog_old")
    // */
    // public function addArticle(Request $request)
    // {
    //     //On crée une variable post qui de base sera null
    //     $post = null;
    //     //On récupère les valeurs des différents input en utilisant la requestpost
    //     $title = $request->get("title");
    //     $author = $request->get("author");
    //     $postDate = $request->get("postdate");
    //     $content = $request->get("content");
    //     $imgPath = $request->get("imgPath");
    //     //On vérifie que chaque input contenait bien une valeur
    //     if ($author && $title && $content) {
    //         //Si c'est le cas, on utilise ces valeurs pour créer une 
    //         //nouvelle instance de Person qu'on met dans la variable
    //         $post = new Post($title,$author , $postDate, $content, $imgPath );
    //         //Qu'on affiche dans la console avec un dump

    //         $repo = new PostRepository();
    //         $repo->add($post);
    //     }
    //     //On fait un render du template en lui donnant à manger la
    //     //variable person qui contient soit une instance soit rien
    //     return $this->render("add.html.twig", [
    //         'post' => $post
    //     ]);
    // }

    /**
     * @Route("/", name="blog")
     */
    public function index()
    {
        $repo = new PostRepository();
        $posts = $repo->findAll();
        return $this->render('post.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * @Route("/del/{id}", name="deletepost")
     */
    public function delete(int $id)
    {
        $repo = new PostRepository();
        $repo->deleteArticle($id);
        return $this->redirectToRoute('blog');
    }

    /**
     * @Route("/add", name="add")
     */
    public function formSymfony(Request $request)
    {
        //On crée unepostRepository $repo instance de notre formulaire en lui indiquant entre
        //les parenthèses quelle formulaire on souhaite générer
        $post = $this->createForm(PostType::class);
        //On donne la request à manger au formulaire pour qu'il traite
        //les données contenue dans celle ci
        $post->handleRequest($request);
        //On indique ce que l'on fera si le formulaire a été soumis
        //et si celui ci est valide
        if ($post->isSubmitted() && $post->isValid()) {
            //On récupère les données du formulaire (ici, une instance
            //de Person)
            dump($post->getData());
        }

        return $this->render('add.html.twig', [
            'post' => $post->createView()
        ]);
    }
}
