<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Entity\Post;
/**
 * Les Form ou Type symfony nous permettrons de générer automatiquement
 * des formulaires qui pourront être géré par symfony plus rapidement
 * et simplement que des formulaires fait à la main
 */
class PostType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**
         * On utilise le builder pour indiquer les différents champs
         * de notre formulaire, ceux ci doivent correspondre à la
         * classe/entity à laquelle on va lié ce formulaire
         */
        $builder->add('title')
                ->add('author')
                ->add('postDate')
                ->add('content')
                ->add('imgPath');
    }

    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        /**
         * On indique la classe que devra générer ce formulaire. Bien
         * souvent il s'agira d'une entité. Il faut que celle ci ait
         * un constructeur pouvant être appelé sans argument ainsi que
         * des setter pour chaque champ indiqué dans le builder
         */
        $resolver->setDefaults([
            'data_class' => Post::class
        ]);
    }
}
