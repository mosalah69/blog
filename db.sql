
CREATE TABLE post(
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(64),
    author VARCHAR(64),
    postDate DATETIME ,
    content TEXT,
    imgPath Varchar(255)
);

INSERT INTO post (title, author, content) VALUES("LONDRES", "Mosalah", "Londres, la capitale de l'Angleterre et du Royaume-Uni, est une ville moderne dont l'histoire remonte à l'époque romaine. En son centre se dressent l'imposant Parlement, l'emblématique Big Ben et l'abbaye de Westminster, lieu de couronnement des monarques britanniques. De l'autre côté de la Tamise, le London Eye, la grande roue, offre une vue panoramique sur le South Bank Center, et toute la ville. ","https://cdn.shoplightspeed.com/shops/605079/files/3766218/256x256x1/dutailier-dutailier-cupcake-lit-simple-single-bed.jpg");